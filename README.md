# THIS PIECE OF SOFTWARE IS NO LONGER DEVELOPED

sway-autologin is an automatically locked sway session intended for
use with autologin display managers

## Dependencies

* Any display manager with autologin support
* sway
* swayidle
* swaylock

## How to install

Copy the contents of the `etc` and `usr` folders into the respective
directories on your machine

## How to use

Configure your display manager to use the `sway-autologin` session.
Here's how you can do it for LightDM:

```
# /etc/lightdm/lightdm.conf

[Seat:*]
autologin-user=your-user
autologin-session=sway-autologin
```
